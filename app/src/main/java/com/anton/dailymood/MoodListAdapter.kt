package com.anton.dailymood


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.anton.dailymood.databinding.FragmentMoodItemBinding
import com.anton.dailymood.room.MoodEntity
import java.text.SimpleDateFormat
import java.util.*

class MoodListAdapter(
    private val values: List<MoodEntity>
) : RecyclerView.Adapter<MoodListAdapter.ViewHolder>() {

    private var formatter = SimpleDateFormat("EEEE, d MMMM - HH:mm:ss", Locale.getDefault())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentMoodItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.fragment_mood_item, parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.formatter = formatter
        holder.binding.item = values[position]
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: FragmentMoodItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}
