package com.anton.dailymood

import android.app.*
import android.app.AlarmManager.INTERVAL_DAY
import android.app.AlarmManager.RTC_WAKEUP
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Intent
import android.os.Build
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.room.Room
import com.anton.dailymood.room.Database
import java.util.*

const val DAILY_MOOD_REQUEST_CODE = 30
const val DAILY_MOOD_NOTIFICATION_CHANNEL_ID = "DAILY_MOOD_NOTIFICATION_CHANNEL_ID"
const val MOOD_DATABASE = "daily-mood"

enum class Mood {
    BLISSFUL,
    HAPPY,
    ANGRY,
    IRATE
}

fun AppCompatActivity.setContent(layout: Int): ViewDataBinding {
    return DataBindingUtil.setContentView(this, layout)
}

fun Activity.getDatabase(): Database {
    return Room.databaseBuilder(
        this.applicationContext,
        Database::class.java,
        MOOD_DATABASE
    ).build()
}

class App : Application() {

    companion object {
        @JvmStatic
        @BindingAdapter("android:text")
        fun setText(view: TextView, value: Enum<*>) {
            view.text = value.toString()
            view.tag = value
        }
    }

    override fun onCreate() {
        super.onCreate()

        setRepeatingAlarm()
        createNotificationChannel()
    }

    private fun setRepeatingAlarm() {
        val calendar = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 1)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
        }

        val intent = Intent(this, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            this, DAILY_MOOD_REQUEST_CODE, intent, FLAG_UPDATE_CURRENT
        )
        val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
        alarmManager.setRepeating(RTC_WAKEUP, calendar.timeInMillis, INTERVAL_DAY, pendingIntent)
    }

    private fun createNotificationChannel() {
        val notificationManager: NotificationManager =
            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            println(
                "notification channel id: ${notificationManager.getNotificationChannel(
                    DAILY_MOOD_NOTIFICATION_CHANNEL_ID
                )}"
            )
            val name = getString(R.string.daily_mood_channel_name)
            val descriptionText = getString(R.string.daily_mood_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel =
                NotificationChannel(DAILY_MOOD_NOTIFICATION_CHANNEL_ID, name, importance).apply {
                    description = descriptionText
                }
            notificationManager.createNotificationChannel(channel)
        }
    }
}