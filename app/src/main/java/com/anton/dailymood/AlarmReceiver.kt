package com.anton.dailymood

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_ONE_SHOT
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val notificationId = intent?.extras?.getInt("android.intent.extra.ALARM_COUNT") ?: 0

        context?.let {
            val moodIntent = Intent(context, CreateMoodActivity::class.java).apply {
                flags = FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent = PendingIntent.getActivity(
                context, DAILY_MOOD_REQUEST_CODE, moodIntent, FLAG_ONE_SHOT
            )
            val builder = NotificationCompat.Builder(context, DAILY_MOOD_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_mood_24px)
                .setContentTitle(context.getString(R.string.daily_mood_notification_title))
                .setContentText(context.getString(R.string.daily_mood_notification_text))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
            with(NotificationManagerCompat.from(context)) {
                notify(notificationId, builder.build())
            }
        }
    }
}