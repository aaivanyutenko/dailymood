package com.anton.dailymood

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.anton.dailymood.room.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MoodListFragment : Fragment() {
    private lateinit var recycler: RecyclerView
    private lateinit var db: Database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { db = it.getDatabase() }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_mood_list, container, false)
        if (view is RecyclerView) {
            recycler = view
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        GlobalScope.launch(context = Dispatchers.Main) {
            val result = withContext(Dispatchers.Default) {
                db.moodDao().getAll()
            }
            recycler.adapter = MoodListAdapter(result)
        }
    }
}
