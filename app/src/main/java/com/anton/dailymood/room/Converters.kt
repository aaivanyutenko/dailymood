package com.anton.dailymood.room

import androidx.room.TypeConverter
import com.anton.dailymood.Mood
import java.util.*

class Converters {
    @TypeConverter
    fun fromEnumToString(value: Mood): String {
        return value.toString()
    }

    @TypeConverter
    fun fromStringToEnum(value: String): Mood {
        return Mood.valueOf(value)
    }

    @TypeConverter
    fun fromDateToLong(value: Date): Long {
        return value.time
    }

    @TypeConverter
    fun fromLongToDate(value: Long): Date {
        return Date(value)
    }
}