package com.anton.dailymood.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MoodDao {
    @Query("SELECT * FROM MoodEntity")
    fun getAll(): List<MoodEntity>

    @Insert
    fun insert(moodEntity: MoodEntity)
}