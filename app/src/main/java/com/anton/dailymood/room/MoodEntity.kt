package com.anton.dailymood.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.anton.dailymood.Mood
import java.util.*

@Entity
data class MoodEntity(
    @ColumnInfo(name = "date") val date: Date,
    @ColumnInfo(name = "mood") val mood: Mood,
    @ColumnInfo(name = "sleep") val sleep: Float,
    @PrimaryKey val id: Int? = null
)