package com.anton.dailymood

import android.animation.ValueAnimator
import android.graphics.PorterDuff.Mode.SRC_IN
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.util.TypedValue
import android.widget.RadioButton
import androidx.annotation.AttrRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.SpringAnimation
import com.anton.dailymood.room.Database
import com.anton.dailymood.room.MoodEntity
import kotlinx.android.synthetic.main.activity_create_mood.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class CreateMoodActivity : AppCompatActivity() {

    private var checkedItem: RadioButton? = null
    private lateinit var db: Database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = setContent(R.layout.activity_create_mood)
        binding.executePendingBindings()

        db = getDatabase()
        setupDailyMoodRadioGroup()
        setupDailyMoodDoneButton()
    }

    private fun setupDailyMoodRadioGroup() {
        val activeColor = ContextCompat.getColor(this, R.color.colorAccent)
        val inactiveColor =
            ContextCompat.getColor(this, this.getColorRes(android.R.attr.textColorPrimary))
        today_mood_radio_group.setOnCheckedChangeListener { _, checkedId ->
            checkedItem?.let { radioButton ->
                animate(activeColor, inactiveColor, radioButton)
            }
            checkedItem = findViewById(checkedId)
            checkedItem?.let { radioButton ->
                animate(inactiveColor, activeColor, radioButton)
            }
        }
    }

    private fun setupDailyMoodDoneButton() {
        today_mood_done.setOnClickListener {
            when {
                checkedItem == null -> {
                    today_mood_radio_group.translationY = today_mood_radio_group.height * 0.05f
                    SpringAnimation(
                        today_mood_radio_group,
                        DynamicAnimation.TRANSLATION_Y,
                        0f
                    ).start()
                }
                0.0f.compareTo(today_mood_sleep_rating.rating) == 0 -> {
                    today_mood_sleep_rating.translationX = today_mood_sleep_rating.width * 0.05f
                    SpringAnimation(
                        today_mood_sleep_rating,
                        DynamicAnimation.TRANSLATION_X,
                        0f
                    ).start()
                }
                else -> {
                    GlobalScope.launch {
                        val date = Date()
                        val mood = checkedItem?.tag as Mood
                        val sleep = today_mood_sleep_rating.rating
                        val moodEntity = MoodEntity(date, mood, sleep)
                        db.moodDao().insert(moodEntity)
                    }
                    finish()
                }
            }
        }
    }

    private fun animate(start: Int, end: Int, radioButton: RadioButton) {
        ValueAnimator.ofArgb(start, end).apply {
            duration = 400
            start()
        }.addUpdateListener { animator ->
            val color = animator.animatedValue as Int
            radioButton.setTextColor(color)
            radioButton.compoundDrawables.forEach {
                it?.let { drawable ->
                    drawable.colorFilter =
                        PorterDuffColorFilter(color, SRC_IN)
                }
            }
        }
    }
}

private fun AppCompatActivity.getColorRes(@AttrRes id: Int): Int {
    val resolvedAttr = TypedValue()
    this.theme.resolveAttribute(id, resolvedAttr, true)
    return resolvedAttr.run { resourceId }
}

